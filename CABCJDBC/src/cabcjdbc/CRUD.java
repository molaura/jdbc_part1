/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cabcjdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Laura Martinez &&  Sergi Formatgé
 */
public class CRUD {

    private final String SELECT_ALL_STR = "SELECT * FROM Companyia ORDER BY 1";
    private final String SELECT_BY_NAME_STR = "SELECT * FROM Companyia WHERE nom = ?";
    private final String INSERT_STR = "INSERT INTO Companyia(nom) " + "VALUES (?)";
    private final String DELETE_STR = "DELETE FROM Companyia WHERE id = ?";

    /**
     *
     * @param companyia
     * @return 
     */
    public int create(Companyia companyia) {
        int rowsUpdated = 0;
        try (PreparedStatement query = Connection.getConnection().prepareStatement(INSERT_STR)) {
            query.setString(1, companyia.getNom());
            rowsUpdated = query.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (int) rowsUpdated;
    }

    public Companyia getByName(String nom) {
        Map<String, Companyia> map = null;
        try (PreparedStatement query = Connection.getConnection().prepareStatement(SELECT_BY_NAME_STR)) {
            query.setString(1, nom);
            map = mapQueryToMapString(query);
        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map == null ? null : map.get(nom);
    }

    public boolean delete(int id) {
        int rowsUpdated = 0;
        try (PreparedStatement query = Connection.getConnection().prepareStatement(DELETE_STR)) {
            query.setInt(1, id);
            rowsUpdated = query.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rowsUpdated > 0;
    }
    
    private Map<String, Companyia> mapQueryToMapString(PreparedStatement query) {
         Map<String, Companyia> map = null;
        Companyia companyia;

        try (ResultSet results = query.executeQuery()) {
            map = new TreeMap<>();
            while (results.next()) {
                companyia = new Companyia(results.getInt("id"), results.getString("nom"));

                map.put(results.getString("nom"), companyia);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    
    }

    private Map<Integer, Companyia> mapQueryToMapInteger(PreparedStatement query) {
        Map<Integer, Companyia> map = null;
        Companyia companyia;

        try (ResultSet results = query.executeQuery()) {
            map = new TreeMap<>();
            while (results.next()) {
                companyia = new Companyia(results.getInt("id"), results.getString("nom"));

                map.put(results.getInt("id"), companyia);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

    public Map<Integer, Companyia> getAll() {
        Map<Integer, Companyia> map = null;
        try (PreparedStatement query = Connection.getConnection().prepareStatement(SELECT_ALL_STR)) {
            map = mapQueryToMapInteger(query);
        } catch (SQLException ex) {
            Logger.getLogger(Companyia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
    }

}
