/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cabcjdbc;

import java.util.Scanner;
import cabcjdbc.Connection;

/**
 *
 * @author Laura Martinez &&  Sergi Formatgé
 */
public class CABCJDBC {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);
        CRUD crud = new CRUD();
        Boolean end = false;
        
        while (end == false){
        System.out.println("*************************************************");
        System.out.println("|               ESCULL UNA OPCIÓ                |");
        System.out.println("*************************************************");
        System.out.println("| Opcions:                                      |");
        System.out.println("|        1. Mostrar totes les companyies        |");
        System.out.println("|        2. Mostrar una companyia pel nom       |");
        System.out.println("|        3. Afegir una nova companyia           |");
        System.out.println("|        4. Esborra companyia                   |");
        System.out.println("|        5. Sortir                              |");
        System.out.println("*************************************************");

        System.out.print("Introdueix una opció: ");

        int opcio = lector.nextInt();
            switch (opcio) {
                case 1:
                    System.out.println(crud.getAll());
                    break;
                case 2:
                    System.out.println("Escriu el nom de la companyia que vols mostrar: ");
                    String nom = lector.next();
                    System.out.println(crud.getByName(nom));
                    break;
                case 3:
                    System.out.println("Entra el nom de la companyia que vols crear: ");
                    String nomCreate = lector.next();
                    Companyia companyia = new Companyia();
                    companyia.setNom(nomCreate);
                    System.out.println("Companyia creada");
                    System.out.println(crud.create(companyia));
                    break;
                case 4:
                    System.out.println("Entra l'id de la companyia que vols eliminar: ");
                    int idDelete = lector.nextInt();
                    crud.delete(idDelete);
                    System.out.println("S'ha eliminat correctament!!!");
                    break;
                case 5:
                    System.out.println("AAAADDDEEEEUUUU!!!");
                    end = true;
                    System.exit(0);
                    break;
                default:
                    System.out.println("Aquesta opció no és valida");
                    break;
            }
        }
    }
}
